prompt --application/pages/page_00002
begin
wwv_flow_api.create_page(
 p_id=>2
,p_user_interface_id=>wwv_flow_api.id(1544341386754987)
,p_name=>'TEST UPLOAD'
,p_step_title=>'TEST UPLOAD'
,p_allow_duplicate_submissions=>'N'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'document.querySelector(''form'').addEventListener(''submit'', (e) => {',
' console.log(e);',
'    console.log(new FormData(document.querySelector(''form'')))',
' return false;',
'});'))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'DEV'
,p_last_upd_yyyymmddhh24miss=>'20191229152352'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(12380578412013101)
,p_plug_name=>'New'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(1465716327754911)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>'<input type="submit" target="x" /><iframe id="x" name="x"></iframe>'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(12381021550013106)
,p_name=>'New'
,p_template=>wwv_flow_api.id(1465716327754911)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_application_temp_files'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(1491466890754925)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381172734013107)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381291032013108)
,p_query_column_id=>2
,p_column_alias=>'APPLICATION_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Application Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381369167013109)
,p_query_column_id=>3
,p_column_alias=>'NAME'
,p_column_display_sequence=>3
,p_column_heading=>'Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381446177013110)
,p_query_column_id=>4
,p_column_alias=>'FILENAME'
,p_column_display_sequence=>4
,p_column_heading=>'Filename'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381564343013111)
,p_query_column_id=>5
,p_column_alias=>'MIME_TYPE'
,p_column_display_sequence=>5
,p_column_heading=>'Mime Type'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(12381695978013112)
,p_query_column_id=>6
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>6
,p_column_heading=>'Created On'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(12380776275013103)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(12380578412013101)
,p_button_name=>'New'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(1521820037754946)
,p_button_image_alt=>'New'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(12382740819013123)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(12380578412013101)
,p_button_name=>'CustomSubmit'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(1521820037754946)
,p_button_image_alt=>'Custom Submit'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(12380671211013102)
,p_name=>'P2_NEW'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(12380578412013101)
,p_prompt=>'New'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(1520771648754943)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'APEX_APPLICATION_TEMP_FILES'
,p_attribute_09=>'SESSION'
,p_attribute_10=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(12814085148069003)
,p_name=>'P2_DATA1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(12380578412013101)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(1520771648754943)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(12814166857069004)
,p_name=>'P2_DATA2'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(12380578412013101)
,p_prompt=>'New'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(1520771648754943)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(12380892620013104)
,p_name=>'New'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(12380776275013103)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(12381832155013114)
,p_event_id=>wwv_flow_api.id(12380892620013104)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var a1 = new Array();',
'',
'/*for (var i =0; i < 10000; i++){',
'    a1.push("ALA MA KOTA NR" + i);',
'}*/',
'',
'apex.server.process ( "TEST", {',
'        x01: "test",',
'        f01: a1,',
'        pageItems: "#P2_NEW"',
'  },{   dataType: "text",',
'        success: function( pData ){',
'             console.log(pData); ',
'        }',
'  } );'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(12381965039013115)
,p_name=>'New_1'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'apexbeforepagesubmit'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(12382033705013116)
,p_event_id=>wwv_flow_api.id(12381965039013115)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'console.log(this);'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(12382819292013124)
,p_name=>'New_2'
,p_event_sequence=>30
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(12382740819013123)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(12382985591013125)
,p_event_id=>wwv_flow_api.id(12382819292013124)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var s = ''Sample file content'';',
'',
'var formData = new FormData();',
'formData.append(''p_json'', ''{"pageItems":{"itemsToSubmit":[{"n":"P2_NEW","v":"C:\\\\fakepath\\\\plik.txt","fileIndex":1,"fileCount":1}],"protected":"''+ $(''#pPageItemsProtected'').val() +''","rowVersion":"","formRegionChecksums":[]},"salt":"''+$(''#pSalt'')'
||'.val()+''"}'');',
'formData.append(''p_flow_id'', ''&APP_ID.'');',
'formData.append(''p_flow_step_id'', 2);',
'formData.append(''p_instance'', ''&APP_SESSION.'');',
'formData.append(''p_debug'', '''');',
'formData.append(''p_request'', ''UPLOAD'');',
'formData.append(''p_reload_on_submit'', ''S'');',
'formData.append(''p_page_submission_id'', $(''#pPageSubmissionId'').val());',
'formData.append(''p_files'', new File([new Blob([s])], ''plik.txt''));',
'//formData.append(''x01'', ''X01 PARAM'');',
'console.log(formData);',
'',
'$.ajax({',
' type: "POST",',
' url: "wwv_flow.accept",',
' data: formData,',
' dataType: "text",',
' processData: false,',
' contentType: false,',
' success: function(data){',
'    console.log(data);     ',
'}',
'});',
''))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(12383401960013130)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'New_1'
,p_process_sql_clob=>'htp.p(''{status: "OK"}'');'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'UPLOAD'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(12382297686013118)
,p_process_sequence=>10
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'New'
,p_process_sql_clob=>'htp.p(apex_application.f01.count);'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
