prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_190100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2019.03.31'
,p_release=>'19.1.0.00.15'
,p_default_workspace_id=>1260514089020361
,p_default_application_id=>101
,p_default_owner=>'DEV'
);
end;
/
 
prompt APPLICATION 101 - TEST
--
-- Application Export:
--   Application:     101
--   Name:            TEST
--   Date and Time:   04:52 Monday January 13, 2020
--   Exported By:     3.91.43.76
--   Flashback:       0
--   Export Type:     Application Export
--   Version:         19.1.0.00.15
--   Instance ID:     250157796364266
--

-- Application Statistics:
--   Pages:                      6
--     Items:                    7
--     Processes:                7
--     Regions:                  7
--     Buttons:                  4
--     Dynamic Actions:          3
--   Shared Components:
--     Logic:
--       Processes:              1
--     Navigation:
--       Lists:                  2
--       Breadcrumbs:            1
--         Entries:              2
--     Security:
--       Authentication:         1
--       Authorization:          1
--     User Interface:
--       Themes:                 1
--       Templates:
--         Page:                 9
--         Region:              16
--         Label:                7
--         List:                12
--         Popup LOV:            1
--         Calendar:             1
--         Breadcrumb:           1
--         Button:               3
--         Report:              10
--       LOVs:                   1
--       Shortcuts:              1
--     Globalization:
--     Reports:
--     E-Mail:
--   Supporting Objects:  Excluded

